package aryo.muhammad.app;

import aryo.muhammad.element.*;
import aryo.muhammad.frames.CentralFrame;
import java.awt.Color;

public class FirstApp 
{
  public static void main(String[] args)
  {

    Manager theManager = null;
    CentralFrame theFrame = null;
    Icon [] theIcons = null;
    Field theField = null;
    Home theHome = null;
    Storage theStorage = null;

    // membuat objek manager
    theManager = new Manager();
    // membuat objek frame, yang memiliki akses ke manager dan manager
    // bisa akses ke frame. frame dengan manager saling membutuhkan
    theFrame = new CentralFrame(theManager); 

    // membuat theFrame sebagai frame
    theManager.setTheFrame(theFrame);

    // mengatur properti frame
    theFrame.setSize(370, 390);
    theFrame.setResizable(false);
    theFrame.setVisible(true);

    theManager.theColors[0] = Color.GREEN; // menentukan urutan warna
    theManager.theColors[1] = Color.YELLOW;
    theManager.theColors[2] = Color.BLUE;
    theManager.theColors[3] = Color.RED;

    for (int i=0; i<theManager.nrOfPlayers; i++)
    {
      
      theStorage = new Storage(); // membuat storage atau tempat awal ikon
      theStorage.setContent((byte) 4);
      theStorage.setTheID((byte) (i * 20 + 1));
      theStorage.setTheLocation(theManager.theFields[i][0]);
      theStorage.setTheOwner(theManager.thePlayers[i]);
      theManager.playingFields[i][0] = theStorage;
      theManager.getTheFields()[i][0].setText("4");
      theManager.getTheFields()[i][0].setEditable(false);

      theHome = new Home(); // membuat tempat menang
      theHome.setContent((byte) 0);
      theHome.setTheID((byte) (i * 20 + 20));
      theHome.setTheLocation(theManager.theFields[i][19]);
      theHome.setTheOwner(theManager.thePlayers[i]);
      theManager.playingFields[i][19] = theHome;
      theManager.getTheFields()[i][19].setText("");
      theManager.getTheFields()[i][19].setEditable(false);

      theManager.thePlayers[i] = new Player(); //instantiation
      theManager.thePlayers[i].setTheID((byte) i);
      theManager.thePlayers[i].setFinished(false);
      theManager.thePlayers[i].setTheColor(theManager.theColors[i]);
      theManager.thePlayers[i].setTheHome(theHome);
      theManager.thePlayers[i].setTheStorage(theStorage);
    }

    for (int i=0; i<theManager.nrOfPlayers; i++)
    {
      for (int j=1; j<14; j++)
      {
        theField = new Field();
        theField.setContent((byte) 0);
        theField.setSaveArea(false);
        theField.setTheID((byte) (i*20+j+1)); // 0*20+1+1 = 2 | dst
        theField.setTheLocation(theManager.theFields[i][j]);
        theField.setTheOwner(theManager.thePlayers[i]);
        theManager.playingFields[i][j] = theField;
        theManager.getTheFields()[i][j].setText("");
        theManager.getTheFields()[i][j].setBackground(Color.white);
        theManager.getTheFields()[i][j].setEditable(false);
        
      }

      for (int j=14; j<19; j++)
      {
        theField = new Field();
        theField.setContent((byte) 0);
        theField.setSaveArea(true);
        theField.setTheID((byte) (i*20+j+1));
        theField.setTheLocation(theManager.theFields[i][j]);
        theField.setTheOwner(theManager.thePlayers[i]);
        theManager.playingFields[i][j] = theField;
        theManager.getTheFields()[i][j].setText("");
        theManager.getTheFields()[i][j].setEditable(false);
      }
    }

    theManager.turnID = (byte) Math.round(Math.random() * theManager.nrOfPlayers - 0.5);
    System.out.println("Turn ID = " + theManager.turnID);
    theManager.changeColorTurn(theManager.theColors[theManager.turnID]);
  }

}