package aryo.muhammad.element;

import javax.swing.JTextField;

public class Field
{
  JTextField theLocation = null;
  byte theID = 0;
  boolean saveArea = false;
  Player theOwner = null;
  byte nrOfIcons = 0;
  Icon theIcons[] = null;
  byte content = 0;

  public Field()
  {
    super();
  }

  public byte getContent()
  {
    return content;
  }

  public void setContent(byte content)
  {
    this.content = content;
  }

  public byte getNrOfIcons()
  {
    return nrOfIcons;
  }

  public void setNrOfIcons(byte nrOfIcons)
  {
    this.nrOfIcons = nrOfIcons;
  }

  public boolean isSaveArea()
  {
    return saveArea;
  }

  public void setSaveArea(boolean saveArea)
  {
    this.saveArea = saveArea;
  }

  public byte getTheID()
  {
    return theID;
  }

  public void setTheID(byte theID)
  {
    this.theID = theID;
  }

  public Icon[] getTheIcons()
  {
    return theIcons;
  }

  public void setTheIcons(Icon[] theIcons)
  {
    this.theIcons = theIcons;
  }

  public JTextField getTheLocation()
  {
    return theLocation;
  }

  public void setTheLocation(JTextField theLocation)
  {
    this.theLocation = theLocation;
  }

  public Player getTheOwner()
  {
    return theOwner;
  }

  public void setTheOwner(Player theOwner)
  {
    this.theOwner = theOwner;
  }
}
